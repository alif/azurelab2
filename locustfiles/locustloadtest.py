import time
from locust import HttpUser, task, between

class QuickstartUser(HttpUser):
    @task
    def integral(self):
        self.client.get("/integral/0/3.14159265359")