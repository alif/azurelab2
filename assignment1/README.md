# azurelab2

## Run

You need to first install the the dependencies with
>pip -r requirements.txt

You run the microservice with:
>flask --app nimicroservice run

the server will serve on `http://localhost:5000/`

To load test with locust, run the app and then run:
>locust -f locustfiles/locustloadtest.py --host http://localhost:5000 --headless -u 15 -r 1