from flask import Flask
import numpy as np

app = Flask(__name__)

def ni(lower, upper, N):
    # Calculate the width of each subinterval
    width = (upper - lower) / N

    # Initialize the total area
    total_area = 0

    # Iterate over each subinterval
    for i in range(N):
        # Calculate the midpoint of the subinterval
        center = lower + (i * width)/2

        # Calculate the area of the rectangle at the midpoint
        area = width * abs(np.sin(center))

        # Add the area of the rectangle to the total area
        total_area += area

    return total_area


@app.route("/integral/<lower>/<upper>")
def numerical_integration(lower, upper):
    l = float(lower)
    u = float(upper)
    integration_results = []
    for N in [10, 100, 1000, 10000, 100000, 1000000]:
        integral = ni(l, u, N)
        print(f'Integral for N={N} is {integral}')
        integration_results.append(integral)
    response_string = "Integration results: \n"
    for N, integral in zip([10, 100, 1000, 10000, 100000, 1000000], integration_results):
        response_string += f"N = {N:7}: {integral}\n"

    return response_string

@app.route("/health")
def health_check():
    return "OK"