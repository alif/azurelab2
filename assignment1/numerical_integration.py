import numpy as np

def numerical_integration(lower, upper, N):
    # Calculate the width of each subinterval
    width = (upper - lower) / N

    # Initialize the total area
    total_area = 0

    # Iterate over each subinterval
    for i in range(N):
        # Calculate the midpoint of the subinterval
        center = lower + (i * width)/2

        # Calculate the area of the rectangle at the midpoint
        area = width * abs(np.sin(center))

        # Add the area of the rectangle to the total area
        total_area += area

    return total_area

# Set the lower and upper bounds of the interval
lower = 0
upper = np.pi

# Perform numerical integration for different values of N
integration_results = []
for N in [10, 100, 1000, 10000, 100000, 1000000]:
    integral = numerical_integration(lower, upper, N)
    print(f'Integral for N={N} is {integral}')
    integration_results.append(integral)

# Print the integration results for each value of N
print("Integration results:")
for N, integral in zip([10, 100, 1000, 10000, 100000, 1000000], integration_results):
    print(f"N = {N:7}: {integral}")
