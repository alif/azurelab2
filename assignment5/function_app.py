import azure.functions as func
import azure.durable_functions as df
from azure.identity import DefaultAzureCredential, UsernamePasswordCredential
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient

myApp = df.DFApp(http_auth_level=func.AuthLevel.ANONYMOUS)

# An HTTP-Triggered Function with a Durable Functions Client binding
@myApp.route(route="orchestrators/{functionName}")
@myApp.durable_client_input(client_name="client")
async def http_start(req: func.HttpRequest, client):
    function_name = req.route_params.get('functionName')
    instance_id = await client.start_new(function_name)
    response = client.create_check_status_response(req, instance_id)
    return response

# Orchestrator
@myApp.orchestration_trigger(context_name="context")
def hello_orchestrator(context):
    #Input read: 
    input_tasks = []
    input_tasks.append(context.call_activity("GetInputDataFn", "mrinput-1.txt"))
    input_tasks.append(context.call_activity("GetInputDataFn", "mrinput-2.txt"))
    input_tasks.append(context.call_activity("GetInputDataFn", "mrinput-3.txt"))
    input_tasks.append(context.call_activity("GetInputDataFn", "mrinput-4.txt"))
    results = yield context.task_all(input_tasks)
    flattened_results = [x for xs in results for x in xs]
    print("Read results")
    print(flattened_results)
    #Map
    map_tasks = []
    for line in flattened_results:
        map_tasks.append(context.call_activity("Mapper", line))
    mapping_results = yield context.task_all(map_tasks)
    flattened_mapping_results = [x for xs in mapping_results for x in xs]
    print("Mapping results")
    print(flattened_mapping_results)
    #Shuffle
    shuffling_results = yield context.call_activity("Shuffler", flattened_mapping_results)
    print("Shuffling results")
    print(shuffling_results)
    #Reduce
    reduce_tasks = []
    for semiwordcount in shuffling_results:
        reduce_tasks.append(context.call_activity("Reducer", semiwordcount))
    reducing_results = yield context.task_all(reduce_tasks)
    print("Reducing results")
    print(reducing_results)
    return reducing_results

#Read from blobstore input activity
@myApp.activity_trigger(input_name="blobname")
def GetInputDataFn(blobname: str):
    connectionstring = "DefaultEndpointsProtocol=https;AccountName=nidurablefunction;AccountKey=VQsB9wU3/BegDuXuqgU2NCmy6L+VIJlradVfZrZhg/XS6OIPAZ3U8NEc+8gCJRYcLgygJsFCae5x+AStfOzZTg==;EndpointSuffix=core.windows.net"
    containername = "importantfiles"
    try:
        print("Azure Blob Storage Python quickstart sample")
        client = BlobServiceClient.from_connection_string(connectionstring)
        importantfilecontainerclient = client.get_container_client(containername)
        blobclient = importantfilecontainerclient.get_blob_client(blobname)
        blobcontent = blobclient.download_blob().readall().decode("utf-8")
        splitblobcontent = blobcontent.split('\n')
        return [(i, line) for i, line in enumerate(splitblobcontent)]
    except Exception as ex:
        print('Exception:')
        print(ex)

@myApp.activity_trigger(input_name="lineitem")
def Mapper(lineitem: tuple):
    words = []
    for word in lineitem[1].split(" "):
        words.append((word, 1))
    return words


@myApp.activity_trigger(input_name="wordlist")
def Shuffler(wordlist: list):
    counts_dict = dict()
    for word in wordlist:
        if(word[0] not in counts_dict):
            counts_dict[word[0]] = [1]
        else:
            counts_dict[word[0]].append(1)
    return [(k, v) for k, v in counts_dict.items()]

@myApp.activity_trigger(input_name="semiwordcount")
def Reducer(semiwordcount: list):
    return [semiwordcount[0], len(semiwordcount[1])]