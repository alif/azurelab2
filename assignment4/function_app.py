import azure.functions as func
import logging
import numpy as np

app = func.FunctionApp(http_auth_level=func.AuthLevel.ANONYMOUS)

def ni(lower, upper, N):
    # Calculate the width of each subinterval
    width = (upper - lower) / N

    # Initialize the total area
    total_area = 0

    # Iterate over each subinterval
    for i in range(N):
        # Calculate the midpoint of the subinterval
        center = lower + (i * width)/2

        # Calculate the area of the rectangle at the midpoint
        area = width * abs(np.sin(center))

        # Add the area of the rectangle to the total area
        total_area += area

    return total_area


@app.route(route="integral/{low}/{up}")
def niaf(req: func.HttpRequest) -> func.HttpResponse:
    l = float(req.route_params.get('low'))
    u = float(req.route_params.get('up'))
    integration_results = []
    for N in [10, 100, 1000, 10000, 100000, 1000000]:
        integral = ni(l, u, N)
        print(f'Integral for N={N} is {integral}')
        integration_results.append(integral)
    response_string = "Integration results: \n"
    for N, integral in zip([10, 100, 1000, 10000, 100000, 1000000], integration_results):
        response_string += f"N = {N:7}: {integral}\n"

    logging.info(f'Python HTTP trigger function processed a request. With low {l} high {u}')

    return func.HttpResponse(response_string)